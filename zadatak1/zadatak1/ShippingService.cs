﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak1
{
    class ShippingService
    {
        private double priceForMass;

        public ShippingService(double priceforMass)
        {
            this.priceForMass = priceforMass;
        }

        public double PriceForMass { get { return this.priceForMass; } } 

        internal double shipping(double weight)
        {
            return this.priceForMass * weight;
        }
    }
}
