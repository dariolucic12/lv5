﻿using System;

namespace zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            Product inception = new Product("Leonardo Di Caprio", 10, 3);
            Box movieBox = new Box("MovieBox");

            movieBox.Add(inception);
            Console.WriteLine(movieBox.Description());
            Console.WriteLine(movieBox.Weight);

            ShippingService service = new ShippingService(0.3);     //zadatak2
            Console.WriteLine(service.shipping(movieBox.Weight));   //zadatak2

        }
    }
}
