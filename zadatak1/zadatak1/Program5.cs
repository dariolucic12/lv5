﻿using System;

namespace zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            BloodTheme bloody = new BloodTheme();
            ReminderNote note = new ReminderNote("Today it will rain", bloody);
            note.Show();
        }
    }
}
